package io.github.mojtab23.keyvalue;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase;
import javafx.collections.ObservableList;

import java.io.IOException;

public class ObservableListSerializer extends AsArraySerializerBase<ObservableList> {
    protected ObservableListSerializer(Class<?> cls, JavaType et, boolean staticTyping, TypeSerializer vts, JsonSerializer<Object> elementSerializer) {
        super(cls, et, staticTyping, vts, elementSerializer);
    }

    @Override
    public AsArraySerializerBase<ObservableList> withResolved(BeanProperty property, TypeSerializer vts, JsonSerializer<?> elementSerializer, Boolean unwrapSingle) {
        return null;
    }

    @Override
    protected void serializeContents(ObservableList value, JsonGenerator gen, SerializerProvider provider) throws IOException {

    }

    @Override
    public boolean hasSingleElement(ObservableList value) {
        return false;
    }

    @Override
    protected ContainerSerializer<?> _withValueTypeSerializer(TypeSerializer vts) {
        return null;
    }
}
