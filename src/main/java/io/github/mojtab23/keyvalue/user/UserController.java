package io.github.mojtab23.keyvalue.user;

import io.github.mojtab23.keyvalue.util.ResponseBody;
import io.github.mojtab23.keyvalue.util.UserIdService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

@RestController()
public class UserController {

    private final UserService userService;
    private final UserIdService userIdService;
//    private TokenStore tokenStore;

    @Autowired
    public UserController(UserService userService, UserIdService userIdService) {
        this.userService = userService;
        this.userIdService = userIdService;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<ResponseBody> signUp(@RequestBody SignUpUserDTO user) {
        final boolean b = userService.signUp(user);
        if (b) {
            return ResponseEntity.ok(ResponseBody.SUCCESS);
        } else {
            return new ResponseEntity<>(
                    ResponseBody.failed("User with this email already exists."),
                    HttpStatus.CONFLICT);
        }

    }

    @GetMapping("/email")
    public ResponseEntity validateEmail(@RequestParam String email) {
        final ValidateEmailDTO validateEmail = userService.validateEmail(email);
        if (validateEmail == null) {
            return ResponseEntity.ok("null");
        }
        return ResponseEntity.ok(validateEmail);
    }

    @PreAuthorize("hasRole(\"USER\")")
    @GetMapping("/user")
    public ResponseEntity<UserDTO> userDetails(OAuth2Authentication authentication) {
        final UserDTO userDTO = userService.userDetails(userIdService.getUserId(authentication));
        return ResponseEntity.ok(userDTO);
    }

    //todo remove this
    @PreAuthorize("hasRole(\"USER\")")
    @GetMapping("/test")
    public ResponseEntity<Authentication> test(Authentication authentication) {
        return ResponseEntity.ok(authentication);
    }
  /*  @GetMapping("/test2")
    public ResponseEntity<ResponseBody> test2() {
        return ResponseEntity.ok(ResponseBody.SUCCESS);
    }
*/



}
