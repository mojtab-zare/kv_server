package io.github.mojtab23.keyvalue.user;

public class SignUpUserDTO {

    private String email;
    private String password;

    public SignUpUserDTO(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public SignUpUserDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "SignUpUserDTO{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
