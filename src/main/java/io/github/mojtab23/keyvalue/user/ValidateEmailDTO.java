package io.github.mojtab23.keyvalue.user;

public class ValidateEmailDTO {


    private boolean emailPattern;
    private boolean emailExists;

    public ValidateEmailDTO(boolean emailPattern, boolean emailExists) {
        this.emailPattern = emailPattern;
        this.emailExists = emailExists;
    }

    public boolean isEmailPattern() {
        return emailPattern;
    }

    public void setEmailPattern(boolean emailPattern) {
        this.emailPattern = emailPattern;
    }

    public boolean isEmailExists() {
        return emailExists;
    }

    public void setEmailExists(boolean emailExists) {
        this.emailExists = emailExists;
    }
}
