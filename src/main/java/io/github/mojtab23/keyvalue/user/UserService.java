package io.github.mojtab23.keyvalue.user;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    private static final String EMAIL_REGEX =
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    public boolean signUp(SignUpUserDTO userDTO) {
        try {
            User user = new User(userDTO.getEmail(), bCryptPasswordEncoder.encode(userDTO.getPassword()));
            userRepository.save(user);
            return true;
        } catch (DuplicateKeyException e) {
            // user exists.
            return false;
        }
    }

    public User findUser(String email) {
        return userRepository.findByEmail(email);
    }

    public ValidateEmailDTO validateEmail(String email) {
        if (checkTextValidation(email)) {
            final User user = findUser(email);
            if (user != null) {
                return new ValidateEmailDTO(false, true);
            } else {
                return null;
            }
        } else {
            return new ValidateEmailDTO(true, false);
        }


    }

    public UserDTO userDetails(ObjectId id) {
        final User user = userRepository.findOne(id);
        if (user != null) {
            return new UserDTO(user.getEmail());
        }
        return null;
    }


    private boolean checkTextValidation(String email) {
        return email.matches(EMAIL_REGEX);
    }

}
