package io.github.mojtab23.keyvalue.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.*;

/**
 * Created by mojtab23 on 8/22/2017.
 * <p>
 * This is a copy of {@link  org.springframework.security.core.userdetails.User}
 * with needed changes
 */


@JsonIgnoreProperties(ignoreUnknown = true)
@Document
public class User implements UserDetails, CredentialsContainer {

    /**
     * email is equals to username
     */
    @Indexed(unique = true)
    private String email;
    private String avatarId;
    @JsonIgnore
    private String lastIPAddress;
    @JsonIgnore
    private Date lastSeen;
    @JsonIgnore
    private boolean accountNonExpired;
    @JsonIgnore
    private Set<SimpleGrantedAuthority> authorities;
    @JsonIgnore
    private boolean accountNonLocked;
    @JsonIgnore
    private boolean credentialsNonExpired;
    @JsonIgnore
    private boolean enabled;
    @JsonIgnore
    private String password;
    @Id
    private ObjectId id;
    @JsonIgnore
    private String validationToken;


    public User() {
    }


    /**
     * used for sign-up
     *
     * @param email
     * @param password
     */
    public User(String email, String password) {
        this.email = email;
        this.password = password;
        final HashSet<SimpleGrantedAuthority> set = new HashSet<>();
        set.add(new SimpleGrantedAuthority("ROLE_USER"));
        authorities = set;
        enabled = true;
        credentialsNonExpired = true;
        accountNonLocked = true;
        accountNonExpired = true;
    }


    private static SortedSet<SimpleGrantedAuthority> sortAuthorities(
            Collection<SimpleGrantedAuthority> authorities) {
        Assert.notNull(authorities, "Cannot pass a null GrantedAuthority collection");
        // Ensure array iteration order is predictable (as per
        // UserDetails.getAuthorities() contract and SEC-717)
        SortedSet<SimpleGrantedAuthority> sortedAuthorities = new TreeSet<SimpleGrantedAuthority>(
                new AuthorityComparator());

        for (SimpleGrantedAuthority grantedAuthority : authorities) {
            Assert.notNull(grantedAuthority,
                    "GrantedAuthority list cannot contain any null elements");
            sortedAuthorities.add(grantedAuthority);
        }

        return sortedAuthorities;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getValidationToken() {
        return validationToken;
    }

    public void setValidationToken(String validationToken) {
        this.validationToken = validationToken;
    }

    public String getLastIPAddress() {
        return lastIPAddress;
    }

    public void setLastIPAddress(String lastIPAddress) {
        this.lastIPAddress = lastIPAddress;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public ObjectId getId() {
        return id;
    }

    public Collection<SimpleGrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<SimpleGrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void eraseCredentials() {
        password = null;
    }


    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        this.avatarId = avatarId;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", avatarId='" + avatarId + '\'' +
                ", lastIPAddress='" + lastIPAddress + '\'' +
                ", lastSeen=" + lastSeen +
                ", accountNonExpired=" + accountNonExpired +
                ", authorities=" + authorities +
                ", accountNonLocked=" + accountNonLocked +
                ", credentialsNonExpired=" + credentialsNonExpired +
                ", enabled=" + enabled +
                ", password='" + password + '\'' +
                ", id=" + id +
                ", validationToken='" + validationToken + '\'' +
                '}';
    }

    private static class AuthorityComparator implements Comparator<GrantedAuthority>,
            Serializable {
        private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

        public int compare(GrantedAuthority g1, GrantedAuthority g2) {
            // Neither should ever be null as each entry is checked before adding it to
            // the set.
            // If the authority is null, it is a custom authority and should precede
            // others.
            if (g2.getAuthority() == null) {
                return -1;
            }

            if (g1.getAuthority() == null) {
                return 1;
            }

            return g1.getAuthority().compareTo(g2.getAuthority());
        }
    }
}

