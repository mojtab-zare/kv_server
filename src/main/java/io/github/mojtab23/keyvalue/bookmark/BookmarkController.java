package io.github.mojtab23.keyvalue.bookmark;


import io.github.mojtab23.keyvalue.util.ResponseBody;
import io.github.mojtab23.keyvalue.util.UserIdService;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("bookmark")
public class BookmarkController {
    private final static Logger LOGGER = LoggerFactory.getLogger(BookmarkController.class);
    private final BookmarkService bookmarkService;
    private final UserIdService userIdService;



    @Autowired
    public BookmarkController(BookmarkService bookmarkService, UserIdService userIdService) {
        this.bookmarkService = bookmarkService;
        this.userIdService = userIdService;
    }


    @PostMapping
    public ResponseEntity<ResponseBody> syncBookmarks(@RequestBody BookmarkTreeNode body, OAuth2Authentication authentication) {
        final ObjectId userId = userIdService.getUserId(authentication);
        bookmarkService.syncBookmarks(body, userId);
        return ResponseEntity.ok(ResponseBody.SUCCESS);
    }

    @GetMapping
    public ResponseEntity getBookmarks(OAuth2Authentication authentication) {
        final List<Bookmark> bookmarks = bookmarkService.getBookmarks(userIdService.getUserId(authentication));
        final ResponseBody<List<Bookmark>> responseBody = ResponseBody.success(bookmarks);
        return ResponseEntity.ok(responseBody);
    }

    @PutMapping("{bookmark_id}")
    public ResponseEntity<ResponseBody<Bookmark>> updateBookmark(
            @PathVariable(name = "bookmark_id") String bookmarkId,
            @RequestBody Bookmark bookmark, OAuth2Authentication authentication) {

        final ObjectId userId = userIdService.getUserId(authentication);
        bookmark.set_id(new ObjectId(bookmarkId));
        bookmark.setUserId(userId);
        final Bookmark updateBookmark = bookmarkService.updateBookmark(bookmark);
        if (updateBookmark != null) {
            return ResponseEntity.ok(ResponseBody.success(updateBookmark));
        } else {
            return ResponseEntity.notFound().build();
        }

    }


}
