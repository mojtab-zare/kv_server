//package io.github.mojtab23.keyvalue.bookmark;
//
//import org.springframework.data.mongodb.core.mapping.Document;
//
//import java.util.Date;
//
//@Document
//public class BookmarkParent {
//    private String title;
//    private Date dateAdded;
//    private Date dateGroupModified;
//
//    public BookmarkParent(String title, Date dateAdded, Date dateGroupModified) {
//        this.title = title;
//        this.dateAdded = dateAdded;
//        this.dateGroupModified = dateGroupModified;
//    }
//
//    public BookmarkParent() {
//    }
//
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public Date getDateAdded() {
//        return dateAdded;
//    }
//
//    public void setDateAdded(Date dateAdded) {
//        this.dateAdded = dateAdded;
//    }
//
//    public Date getDateGroupModified() {
//        return dateGroupModified;
//    }
//
//    public void setDateGroupModified(Date dateGroupModified) {
//        this.dateGroupModified = dateGroupModified;
//    }
//
//
//    @Override
//    public String toString() {
//        return "BookmarkParent{" +
//                " title='" + title + '\'' +
//                ", dateAdded=" + dateAdded +
//                ", dateGroupModified=" + dateGroupModified +
//                '}';
//    }
//
//}
