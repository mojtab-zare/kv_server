package io.github.mojtab23.keyvalue.bookmark;

public interface CustomBookmarkRepository {

    Bookmark updateBookmark(Bookmark bookmark);

}
