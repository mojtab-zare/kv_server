package io.github.mojtab23.keyvalue.bookmark;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
public class Bookmark {

    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;
    @JsonIgnore
    private ObjectId userId;
    @Indexed()
    private List<String> ancestors;
    private String url;
    private String title;
    private Date dateAdded;
    public Bookmark() {
    }

    public Bookmark(String url, String title, Date dateAdded, List<String> ancestors, ObjectId userId) {
        this.url = url;
        this.title = title;
        this.dateAdded = dateAdded;
        this.ancestors = ancestors;
        this.userId = userId;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "_id=" + _id +
                ", userId=" + userId +
                ", ancestors=" + ancestors +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", dateAdded=" + dateAdded +
                '}';
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public List<String> getAncestors() {
        return ancestors;
    }

    public void setAncestors(List<String> ancestors) {
        this.ancestors = ancestors;
    }
}
