package io.github.mojtab23.keyvalue.bookmark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class BookmarkRepositoryImpl implements CustomBookmarkRepository {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public BookmarkRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Bookmark updateBookmark(Bookmark bookmark) {

        final Query query = Query.query(Criteria.where("_id").is(bookmark.get_id()).
                and("userId").is(bookmark.getUserId()));
        final Update update = Update.update("title", bookmark.getTitle()).
                set("url", bookmark.getUrl());
        final Bookmark modify = mongoTemplate.findAndModify(query, update, Bookmark.class);
        return modify;
    }
}
