package io.github.mojtab23.keyvalue.bookmark;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * @see <a href='https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/bookmarks/BookmarkTreeNode' >bookmarks.BookmarkTreeNode</a>
 */
@Document(collection = "bookmark_node")
public class BookmarkTreeNode {


    /**
     * browser ID
     */
    @Id
    private ObjectId _id = new ObjectId();
    private String id;
    private String parentId;
    private int index;
    private String url;
    private String title;
    private Date dateAdded;
    private Date dateGroupModified;
    @DBRef
    private List<BookmarkTreeNode> children;

    public BookmarkTreeNode() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateGroupModified() {
        return dateGroupModified;
    }

    public void setDateGroupModified(Date dateGroupModified) {
        this.dateGroupModified = dateGroupModified;
    }

    public List<BookmarkTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<BookmarkTreeNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "BookmarkTreeNode{" +
                ", id='" + id + '\'' +
                ", parentId=" + parentId +
                ", index=" + index +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", dateAdded=" + dateAdded +
                ", dateGroupModified=" + dateGroupModified +
                ", children=" + children +
                '}';
    }

    public ObjectId get_id() {
        return _id;
    }
}
