package io.github.mojtab23.keyvalue.bookmark;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BookmarkRepository extends MongoRepository<Bookmark, String>, CustomBookmarkRepository {

    public List<Bookmark> findByUserId(ObjectId userId);

}
