package io.github.mojtab23.keyvalue.bookmark;


import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@Service
public class BookmarkService {

    private final static Logger LOGGER = LoggerFactory.getLogger(BookmarkService.class);
    private final BookmarkRepository bookmarkRepository;

    @Autowired
    public BookmarkService(BookmarkRepository bookmarkRepository) {
        this.bookmarkRepository = bookmarkRepository;
    }

    public void syncBookmarks(BookmarkTreeNode body, ObjectId user) {
        final List<Bookmark> bookmarks = walkTree(body, user);
//bookmarkRepository. todo see database and update changes...
        bookmarkRepository.save(bookmarks);
    }

    private List<Bookmark> walkTree(BookmarkTreeNode body, ObjectId user) {
        final List<Bookmark> bookmarks = new ArrayList<>();
        final Stack<String> ancestors = new Stack<>();
        walkRecursive(body, ancestors, bookmarks, user);
        return bookmarks;
    }

    private void walkRecursive(BookmarkTreeNode node, final Stack<String> ancestors,
                               final List<Bookmark> bookmarks, final ObjectId user) {

        final String nodeUrl = node.getUrl();
        if (nodeUrl == null) {// its a folder
            if (node.getChildren() != null && node.getChildren().size() == 0) {
                return;
            }
            ancestors.push(node.getTitle());
            node.getChildren().forEach(bookmarkTreeNode -> walkRecursive(bookmarkTreeNode, ancestors,
                    bookmarks, user));
            ancestors.pop();
        } else {// its child node
            final Bookmark bookmark = new Bookmark(nodeUrl, node.getTitle(), node.getDateAdded(),
                    new ArrayList<>(ancestors), user);
            bookmarks.add(bookmark);
        }

    }

    public List<Bookmark> getBookmarks(ObjectId userId) {
        return bookmarkRepository.findByUserId(userId);
    }

    Bookmark updateBookmark(Bookmark bookmark) {
        return bookmarkRepository.updateBookmark(bookmark);
    }


}
