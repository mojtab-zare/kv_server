package io.github.mojtab23.keyvalue.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .formLogin().and()
                .requestMatchers().antMatchers(/*HttpMethod.OPTIONS, "/oauth/token",*/"/oauth/authorize", "/sign-up","/email"/*,"/test2"*/).and()
                .authorizeRequests()
                .anyRequest().permitAll();
    }


//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.parentAuthenticationManager(authenticationManager)
//                .inMemoryAuthentication()
//                .withUser("mojtaba").password("3141").roles("USER");
//    }
}
