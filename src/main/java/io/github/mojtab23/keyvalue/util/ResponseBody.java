package io.github.mojtab23.keyvalue.util;

public class ResponseBody<T> {

    private static final String SUCCESS_STATUS = "success";
    public static final ResponseBody<Void> SUCCESS = new ResponseBody<>(SUCCESS_STATUS, null);
    private static final String FAILED_STATUS = "failed";
    private String status;
    private T data;

    public ResponseBody(String status, T data) {
        this.status = status;
        this.data = data;
    }

    public ResponseBody() {
    }

    public static <T> ResponseBody<T> success(T data) {
        return new ResponseBody<>(SUCCESS_STATUS, data);
    }

    public static <T> ResponseBody<T> failed(T error) {
        return new ResponseBody<>(FAILED_STATUS, error);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseBody{" +
                "status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
