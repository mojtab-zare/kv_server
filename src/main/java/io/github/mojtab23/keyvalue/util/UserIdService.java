package io.github.mojtab23.keyvalue.util;


import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

@Service
public class UserIdService {


    private final TokenStore tokenStore;

    @Autowired
    public UserIdService(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    public ObjectId getUserId(OAuth2Authentication authentication) {
        final OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
        final OAuth2AccessToken accessToken = tokenStore.readAccessToken(details.getTokenValue());
        final String userId = (String) accessToken.getAdditionalInformation().get("user_id");
        return new ObjectId(userId);
    }

}
