package io.github.mojtab23.keyvalue.tab;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

@Service
public class TabsService {


    private final TabsRepository tabsRepository;

    @Autowired
    public TabsService(TabsRepository tabsRepository) {
        this.tabsRepository = tabsRepository;
    }

    public void setTabs(Tabs tabs) {

        tabsRepository.updateTabs(tabs);

    }

    public Tabs getTabs(ObjectId userId) {

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreNullValues();
        final Tabs tabs = new Tabs();
        tabs.setUserId(userId);
        final Example<Tabs> of = Example.of(tabs, matcher);
//        return tabsRepository.findOne(of);
        return tabsRepository.findFirstByUserId(userId);
    }
}
