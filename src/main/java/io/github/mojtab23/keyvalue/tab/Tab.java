package io.github.mojtab23.keyvalue.tab;

public class Tab {

    private String title;
    private String url;
    private String favIconUrl;


    public Tab(String title, String url, String favIconUrl) {
        this.title = title;
        this.url = url;
        this.favIconUrl = favIconUrl;
    }

    public Tab() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFavIconUrl() {
        return favIconUrl;
    }

    public void setFavIconUrl(String favIconUrl) {
        this.favIconUrl = favIconUrl;
    }

    @Override
    public String toString() {
        return "Tab{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", favIconUrl='" + favIconUrl + '\'' +
                '}';
    }
}
