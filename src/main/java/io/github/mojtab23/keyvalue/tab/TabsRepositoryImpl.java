package io.github.mojtab23.keyvalue.tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class TabsRepositoryImpl implements CustomTabsRepository {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public TabsRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }



    @Override
    public void updateTabs(Tabs tabs) {
        final Query query= Query.query(Criteria.where("userId").is(tabs.getUserId()));
        final Update update = Update.update("tabs", tabs.getTabs());
        mongoTemplate.upsert(query, update, Tabs.class);
    }
}
