package io.github.mojtab23.keyvalue.tab;


import io.github.mojtab23.keyvalue.util.UserIdService;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("tab")
public class TabsController {

    private final static Logger LOGGER = LoggerFactory.getLogger(TabsController.class);

    private final UserIdService userIdService;
    private final TabsService tabsService;


    @Autowired
    public TabsController(UserIdService userIdService, TabsService tabsService) {
        this.userIdService = userIdService;
        this.tabsService = tabsService;
    }

    @PostMapping
    public ResponseEntity setTabs(@RequestBody Tabs tabs, OAuth2Authentication authentication) {
        final ObjectId userId = userIdService.getUserId(authentication);
        tabs.setUserId(userId);
        tabsService.setTabs(tabs);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity getTabs(OAuth2Authentication authentication) {

        final ObjectId userId = userIdService.getUserId(authentication);
        final Tabs tabs = tabsService.getTabs(userId);
        LOGGER.debug("Tabs: {}",tabs);
        return ResponseEntity.ok(tabs);

    }


}
