package io.github.mojtab23.keyvalue.tab;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document
public class Tabs {

    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;
    @JsonIgnore
    @Indexed(unique = true)
    private ObjectId userId;

    private List<Tab> tabs;

    public Tabs(ObjectId userId, List<Tab> tabs) {
        this.userId = userId;
        this.tabs = tabs;
    }

    public Tabs() {
    }

    public ObjectId get_id() {
        return _id;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

    public void setTabs(List<Tab> tabs) {
        this.tabs = tabs;
    }

    @Override
    public String toString() {
        return "Tabs{" +
                "_id=" + _id +
                ", userId=" + userId +
                ", tabs=" + tabs +
                '}';
    }
}
