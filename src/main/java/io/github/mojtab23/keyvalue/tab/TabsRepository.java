package io.github.mojtab23.keyvalue.tab;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TabsRepository extends MongoRepository<Tabs, ObjectId>, CustomTabsRepository {

    Tabs findFirstByUserId(ObjectId userId);

}
